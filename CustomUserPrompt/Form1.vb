﻿
Public Class frmCustomUserPrompt

    Private Const MF_BYPOSITION As Int32 = &H400
    Private Const MF_REMOVE As Int32 = &H1000
    Private Declare Auto Function GetSystemMenu Lib "user32.dll" (ByVal hWnd As IntPtr, ByVal bRevert As Int32) As IntPtr
    Private Declare Auto Function GetMenuItemCount Lib "user32.dll" (ByVal hMenu As IntPtr) As Int32
    Private Declare Function DrawMenuBar Lib "user32.dll" (ByVal hwnd As IntPtr) As Boolean
    Private Declare Auto Function RemoveMenu Lib "user32.dll" (ByVal hMenu As IntPtr, ByVal nPosition As Int32, ByVal wFlags As Int32) As Int32

    Dim intTimeLeft As Integer = 15
    Dim boolFirstLine As Boolean = True
    Dim boolHeightSet As Boolean = False
    Dim boolWidthSet As Boolean = False
    Dim intFormHeight As Integer
    Dim intFormWidth As Integer
    Dim boolBtnAdded As Boolean = False
    Dim boolResponse As Boolean = False
    Dim boolImg As Boolean = False
    Dim boolHelp As Boolean = False

    'To return input to the command window you run the program from, use "start /WAIT /B (path to program)"
    Private Sub FrmCustomUserPrompt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strCommandLine As String
        strCommandLine = Command()
        If strCommandLine <> String.Empty Then
            If strCommandLine = "/help" Or strCommandLine = "/?" Then
                Console.Write("To customize this form, use the following parameters followed by the value for each surrounded by quotation marks:" & Environment.NewLine & "-hdr: Change the header text" & Environment.NewLine & "-text: Set the body text.  Include \n to start a new line." & Environment.NewLine & "-btn: Add button(s) to the form.  Include \n to add different buttons" & Environment.NewLine & "-img: Display an image from a file path." & Environment.NewLine & "-icon: Set a custom form icon from a file path." & Environment.NewLine & "-time: Set the time in seconds until the form expires.  Must be a whole number.  Set to 0 to disable timer" & Environment.NewLine & "-height: Set the height of the form.  Must be a whole number greater than 170" & Environment.NewLine & "-width: Set the width of the form.  Must be a whole number greater than 300" & Environment.NewLine & "-ontop:  Set to 1 to force the form on top of other windows" & Environment.NewLine & "-noxbtn:  Set to 1 to disable the X button on the form." & Environment.NewLine & "Example: start /WAIT /B (path to program) -hdr ""Example Header"" -text ""Line 1\nLine 2"" -height ""250"" -btn ""Yes\nNo""")
                boolResponse = True
                boolHelp = True
                Me.Close()
            Else
                Dim strArray() As String = Split(strCommandLine, """")

                If strArray.Count > 1 Then

                    For i = 1 To (strArray.Count - 1)

                        If (strArray(i - 1) = "-text ") Or (strArray(i - 1) = " -text ") Then
                            Dim strBody() As String = Split(strArray(i), "\n")
                            For l = 0 To (strBody.Count - 1)
                                If boolFirstLine = True Then
                                    lblBody.Text = strBody(l)
                                    boolFirstLine = False
                                Else
                                    lblBody.Text += Environment.NewLine & strBody(l)
                                End If
                            Next
                        End If

                        If (strArray(i - 1) = "-btn ") Or (strArray(i - 1) = " -btn ") Then
                            Dim strButtons() As String = Split(strArray(i), "\n")
                            For b = 0 To (strButtons.Count - 1)
                                AddNewButton(strButtons(b))
                            Next
                            If boolBtnAdded = False Then
                                boolBtnAdded = True
                            End If
                        End If

                        If (strArray(i - 1) = "-hdr ") Or (strArray(i - 1) = " -hdr ") Then
                            Me.Text = strArray(i)
                        End If

                        If (strArray(i - 1) = "-time ") Or (strArray(i - 1) = " -time ") Then
                            If IsNumeric(strArray(i)) Then
                                If (Math.Floor(CDec(strArray(i))) = Math.Ceiling(CDec(strArray(i)))) And CInt(strArray(i)) > 0 Then
                                    intTimeLeft = CInt(strArray(i))
                                    lblTime.Text = TimeDisplay(intTimeLeft)
                                ElseIf CInt(strArray(i)) = 0 Then
                                    Timer1.Enabled = False
                                    lblTime.Enabled = False
                                    lblTime.Visible = False
                                End If
                            Else
                                lblTime.Text = TimeDisplay(intTimeLeft)
                            End If
                        ElseIf Timer1.Enabled = True Then
                            lblTime.Text = TimeDisplay(intTimeLeft)
                        End If

                        If (strArray(i - 1) = "-height ") Or (strArray(i - 1) = " -height ") Then
                            If IsNumeric(strArray(i)) Then
                                If (Math.Floor(CDec(strArray(i))) = Math.Ceiling(CDec(strArray(i)))) And CInt(strArray(i)) > 170 Then
                                    intFormHeight = CInt(strArray(i))
                                    boolHeightSet = True
                                End If
                            End If
                        End If

                        If (strArray(i - 1) = "-width ") Or (strArray(i - 1) = " -width ") Then
                            If IsNumeric(strArray(i)) Then
                                If (Math.Floor(CDec(strArray(i))) = Math.Ceiling(CDec(strArray(i)))) And CInt(strArray(i)) > 300 Then
                                    intFormWidth = CInt(strArray(i))
                                    boolWidthSet = True
                                End If
                            End If
                        End If


                        If (strArray(i - 1) = "-ontop ") Or (strArray(i - 1) = " -ontop ") Then
                            If strArray(i).ToString = "1" Then
                                Me.TopMost = True
                            Else
                                Me.TopMost = False
                            End If
                        End If

                        If (strArray(i - 1) = "-noxbtn ") Or (strArray(i - 1) = " -noxbtn ") Then
                            If strArray(i) = "1" Then
                                DisableCloseButton(Me.Handle)
                            End If
                        End If

                        If (strArray(i - 1) = "-img ") Or (strArray(i - 1) = " -img ") Then
                            If boolImg = False Then
                                Try
                                    picImage.Image = Image.FromFile(strArray(i))
                                    picImage.Visible = True
                                    boolImg = True
                                Catch ex As Exception

                                End Try
                            End If
                        End If

                        If (strArray(i - 1) = "-icon ") Or (strArray(i - 1) = " -icon ") Then
                            Try
                                Me.Icon = New System.Drawing.Icon(strArray(i))
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                    If boolHeightSet = True Then
                        Me.Height = intFormHeight
                    End If
                    If boolWidthSet = True Then
                        Me.Width = intFormWidth
                    End If
                    If boolBtnAdded = False Then
                        tblButtons.Enabled = False
                        tblButtons.Visible = False
                        flwButtons.Enabled = False
                        flwButtons.Visible = False
                        btnReference.Enabled = True
                        btnReference.Visible = True
                        btnReference.AutoSize = True
                        btnReference.Text = "Close (no buttons were added)"
                        btnReference.Anchor = AnchorStyles.Bottom
                    End If
                End If
            End If

        Else
            Timer1.Enabled = False
            MessageBox.Show("No command-line parameters were entered." & Environment.NewLine & "To run this program, open a command prompt window and enter ""start /WAIT /B (path to program) (parameters to use)""" & Environment.NewLine & "To customize this form, use the following parameters followed by the value for each surrounded by quotation marks:" & Environment.NewLine & "-hdr: Change the header text" & Environment.NewLine & "-text: Set the body text.  Include \n to start a new line." & Environment.NewLine & "-btn: Add button(s) to the form.  Include \n to add different buttons" & Environment.NewLine & "-img: Display an image from a file path." & Environment.NewLine & "-icon: Set a custom form icon from a file path." & Environment.NewLine & "-time: Set the time in seconds until the form expires.  Must be a whole number.  Set to 0 to disable timer" & Environment.NewLine & "-height: Set the height of the form.  Must be a whole number greater than 170" & Environment.NewLine & "-width: Set the width of the form.  Must be a whole number greater than 300" & Environment.NewLine & "-ontop:  Set to 1 to force the form on top of other windows" & Environment.NewLine & "-noxbtn:  Set to 1 to disable the X button on the form." & Environment.NewLine & "Example: start /WAIT /B (path to program) -hdr ""Example Header"" -text ""Line 1\nLine 2"" -height ""250"" -btn ""Yes\nNo""" & Environment.NewLine & "Enter ""/help"" or ""/?"" to view the parameters in the command line.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            boolResponse = True
            boolHelp = True
            Console.Write("Program was called with no parameters.")
            Me.Close()
        End If
        If boolHelp = False Then
            Me.CenterToScreen()
        End If


    End Sub

    Private Sub Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim selectedBtn As Button = sender

        Dim strOutput As String = selectedBtn.Text

        Console.Write(strOutput)
        boolResponse = True
        Me.Close()
    End Sub

    Public Function AddNewButton(text As String) As System.Windows.Forms.Button ', pos As Integer) As System.Windows.Forms.Button
        Dim btn As New System.Windows.Forms.Button()
        flwButtons.Controls.Add(btn)
        btn.AutoSize = True
        btn.Text = text
        btn.Anchor = AnchorStyles.Bottom
        AddHandler btn.Click, AddressOf Me.Button_Click

        Return btn
    End Function

    Public Function TimeDisplay(time As Integer) As String
        Dim strTemp As String
        Dim intMin As Integer
        Dim intSec As Integer
        Dim intHr As Integer
        Dim strMin As String
        Dim strSec As String
        Dim strHr As String

        intHr = Math.Floor(time / 3600)
        If intHr < 10 Then
            strHr = "0" & intHr.ToString
        Else
            strHr = intHr.ToString
        End If
        intMin = Math.Floor((time Mod 3600) / 60)
        If intMin < 10 Then
            strMin = "0" & intMin.ToString
        Else
            strMin = intMin.ToString
        End If
        intSec = time Mod 60
        If intSec < 10 Then
            strSec = "0" & intSec.ToString
        Else
            strSec = intSec.ToString
        End If
        strTemp = "Time Left: " & strHr & ":" & strMin & ":" & strSec
        Return strTemp
    End Function

    'This method will be used to disable the Form "X" button
    Public Shared Sub DisableCloseButton(ByVal hwnd As IntPtr)
        Dim hMenu As IntPtr, n As Int32
        hMenu = GetSystemMenu(hwnd, 0)
        If Not hMenu.Equals(IntPtr.Zero) Then
            n = GetMenuItemCount(hMenu)
            If n > 0 Then
                RemoveMenu(hMenu, n - 1, MF_BYPOSITION Or MF_REMOVE)
                RemoveMenu(hMenu, n - 2, MF_BYPOSITION Or MF_REMOVE)
                DrawMenuBar(hwnd)
            End If
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If intTimeLeft > 0 Then
            ' Display the new time left
            ' by updating the Time Left label.
            intTimeLeft -= 1
            lblTime.Text = TimeDisplay(intTimeLeft)
        Else
            Me.Close()
        End If
    End Sub

    Private Sub BtnReference_Click(sender As Object, e As EventArgs) Handles btnReference.Click
        Console.Write("Whoever called the user prompt forgot to add buttons.")
        boolResponse = True
        Me.Close()
    End Sub

    Private Sub FrmCustomUserPrompt_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If boolResponse = False Then
            Console.Write("User closed the program or time ran out.")
            If boolBtnAdded = False Then
                Console.Write("  Not that it matters, since no buttons were added anyway.")
            End If
        End If
    End Sub

    Private Sub FrmCustomUserPrompt_SizeChanged(sender As Object, e As EventArgs) Handles MyBase.SizeChanged

        picImage.Left = (Me.Width / 2) - (picImage.Width / 2)
    End Sub
End Class
