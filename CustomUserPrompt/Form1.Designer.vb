﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomUserPrompt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustomUserPrompt))
        Me.lblBody = New System.Windows.Forms.Label()
        Me.btnReference = New System.Windows.Forms.Button()
        Me.stsStatus = New System.Windows.Forms.StatusStrip()
        Me.lblTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.flwButtons = New System.Windows.Forms.FlowLayoutPanel()
        Me.tblButtons = New System.Windows.Forms.TableLayoutPanel()
        Me.picImage = New System.Windows.Forms.PictureBox()
        Me.stsStatus.SuspendLayout()
        Me.tblButtons.SuspendLayout()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblBody
        '
        Me.lblBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblBody.Location = New System.Drawing.Point(0, 0)
        Me.lblBody.Name = "lblBody"
        Me.lblBody.Padding = New System.Windows.Forms.Padding(0, 0, 0, 35)
        Me.lblBody.Size = New System.Drawing.Size(284, 131)
        Me.lblBody.TabIndex = 0
        Me.lblBody.Text = "(no body text entered)"
        Me.lblBody.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnReference
        '
        Me.btnReference.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnReference.AutoSize = True
        Me.btnReference.Enabled = False
        Me.btnReference.Location = New System.Drawing.Point(62, 81)
        Me.btnReference.Name = "btnReference"
        Me.btnReference.Size = New System.Drawing.Size(161, 23)
        Me.btnReference.TabIndex = 1
        Me.btnReference.Text = "Close (no buttons were added)"
        Me.btnReference.UseVisualStyleBackColor = True
        Me.btnReference.Visible = False
        '
        'stsStatus
        '
        Me.stsStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblTime})
        Me.stsStatus.Location = New System.Drawing.Point(0, 109)
        Me.stsStatus.Name = "stsStatus"
        Me.stsStatus.Size = New System.Drawing.Size(284, 22)
        Me.stsStatus.SizingGrip = False
        Me.stsStatus.TabIndex = 2
        Me.stsStatus.Text = "StatusStrip1"
        '
        'lblTime
        '
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(60, 17)
        Me.lblTime.Text = "Time Left:"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'flwButtons
        '
        Me.flwButtons.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.flwButtons.AutoSize = True
        Me.flwButtons.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.flwButtons.Location = New System.Drawing.Point(142, 3)
        Me.flwButtons.Name = "flwButtons"
        Me.flwButtons.Size = New System.Drawing.Size(0, 27)
        Me.flwButtons.TabIndex = 3
        '
        'tblButtons
        '
        Me.tblButtons.ColumnCount = 1
        Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblButtons.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblButtons.Controls.Add(Me.flwButtons, 0, 0)
        Me.tblButtons.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tblButtons.Location = New System.Drawing.Point(0, 76)
        Me.tblButtons.Name = "tblButtons"
        Me.tblButtons.RowCount = 1
        Me.tblButtons.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblButtons.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblButtons.Size = New System.Drawing.Size(284, 33)
        Me.tblButtons.TabIndex = 4
        '
        'picImage
        '
        Me.picImage.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.picImage.Location = New System.Drawing.Point(92, 0)
        Me.picImage.Name = "picImage"
        Me.picImage.Size = New System.Drawing.Size(100, 50)
        Me.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picImage.TabIndex = 5
        Me.picImage.TabStop = False
        Me.picImage.Visible = False
        '
        'frmCustomUserPrompt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 131)
        Me.Controls.Add(Me.picImage)
        Me.Controls.Add(Me.tblButtons)
        Me.Controls.Add(Me.stsStatus)
        Me.Controls.Add(Me.btnReference)
        Me.Controls.Add(Me.lblBody)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCustomUserPrompt"
        Me.Text = "Custom User Prompt"
        Me.stsStatus.ResumeLayout(False)
        Me.stsStatus.PerformLayout()
        Me.tblButtons.ResumeLayout(False)
        Me.tblButtons.PerformLayout()
        CType(Me.picImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblBody As Label
    Friend WithEvents btnReference As Button
    Friend WithEvents stsStatus As StatusStrip
    Friend WithEvents lblTime As ToolStripStatusLabel
    Friend WithEvents Timer1 As Timer
    Friend WithEvents flwButtons As FlowLayoutPanel
    Friend WithEvents tblButtons As TableLayoutPanel
    Friend WithEvents picImage As PictureBox
End Class
