# Code Examples

These are some examples of the code used in the Custom User Prompt.

## Getting the command line parameters

```
  Private Sub FrmCustomUserPrompt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strCommandLine As String
        strCommandLine = Command()
        If strCommandLine <> String.Empty Then
            If strCommandLine = "/help" Or strCommandLine = "/?" Then
                Console.Write("To customize this form, use the following parameters followed by the value for each surrounded by quotation marks:" & Environment.NewLine & "-hdr: Change the header text" & Environment.NewLine & "-text: Set the body text.  Include \n to start a new line." & Environment.NewLine & "-btn: Add button(s) to the form.  Include \n to add different buttons" & Environment.NewLine & "-img: Display an image from a file path." & Environment.NewLine & "-icon: Set a custom form icon from a file path." & Environment.NewLine & "-time: Set the time in seconds until the form expires.  Must be a whole number.  Set to 0 to disable timer" & Environment.NewLine & "-height: Set the height of the form.  Must be a whole number greater than 170" & Environment.NewLine & "-width: Set the width of the form.  Must be a whole number greater than 300" & Environment.NewLine & "-ontop:  Set to 1 to force the form on top of other windows" & Environment.NewLine & "-noxbtn:  Set to 1 to disable the X button on the form." & Environment.NewLine & "Example: start /WAIT /B (path to program) -hdr ""Example Header"" -text ""Line 1\nLine 2"" -height ""250"" -btn ""Yes\nNo""")
                boolResponse = True
                boolHelp = True
                Me.Close()
```
This gets the input from the command line and puts it into a string.  If the user enters */help* or */?* after the program call, displays the list of parameters in the command window.

##Performing functions based on the parameters used

```
 Else
                Dim strArray() As String = Split(strCommandLine, """")

                If strArray.Count > 1 Then

                    For i = 1 To (strArray.Count - 1)

                        If (strArray(i - 1) = "-text ") Or (strArray(i - 1) = " -text ") Then
                            Dim strBody() As String = Split(strArray(i), "\n")
                            For l = 0 To (strBody.Count - 1)
                                If boolFirstLine = True Then
                                    lblBody.Text = strBody(l)
                                    boolFirstLine = False
                                Else
                                    lblBody.Text += Environment.NewLine & strBody(l)
                                End If
                            Next
                        End If
```
This splits the command string into an array and determines which function to execute based on the parameters used.  This example also shows how the input for the body text is determined and added to the form.

```
If (strArray(i - 1) = "-btn ") Or (strArray(i - 1) = " -btn ") Then
                            Dim strButtons() As String = Split(strArray(i), "\n")
                            For b = 0 To (strButtons.Count - 1)
                                AddNewButton(strButtons(b))
                            Next
                            If boolBtnAdded = False Then
                                boolBtnAdded = True
                            End If
                        End If
```
```
Public Function AddNewButton(text As String) As System.Windows.Forms.Button ', pos As Integer) As System.Windows.Forms.Button
        Dim btn As New System.Windows.Forms.Button()
        flwButtons.Controls.Add(btn)
        btn.AutoSize = True
        btn.Text = text
        btn.Anchor = AnchorStyles.Bottom
        AddHandler btn.Click, AddressOf Me.Button_Click

        Return btn
    End Function
```
The functions for getting the user's custom buttons and adding them to the form.

```
If (strArray(i - 1) = "-time ") Or (strArray(i - 1) = " -time ") Then
                            If IsNumeric(strArray(i)) Then
                                If (Math.Floor(CDec(strArray(i))) = Math.Ceiling(CDec(strArray(i)))) And CInt(strArray(i)) > 0 Then
                                    intTimeLeft = CInt(strArray(i))
                                    lblTime.Text = TimeDisplay(intTimeLeft)
                                ElseIf CInt(strArray(i)) = 0 Then
                                    Timer1.Enabled = False
                                    'lblTime.Text = "Time limit disabled"
                                    lblTime.Enabled = False
                                    lblTime.Visible = False
                                End If
                            Else
                                lblTime.Text = TimeDisplay(intTimeLeft)
                            End If
                        ElseIf Timer1.Enabled = True Then
                            lblTime.Text = TimeDisplay(intTimeLeft)
                        End If
```
```
 Public Function TimeDisplay(time As Integer) As String
        Dim strTemp As String
        Dim intMin As Integer
        Dim intSec As Integer
        Dim intHr As Integer
        Dim strMin As String
        Dim strSec As String
        Dim strHr As String

        intHr = Math.Floor(time / 3600)
        If intHr < 10 Then
            strHr = "0" & intHr.ToString
        Else
            strHr = intHr.ToString
        End If
        intMin = Math.Floor((time Mod 3600) / 60)
        If intMin < 10 Then
            strMin = "0" & intMin.ToString
        Else
            strMin = intMin.ToString
        End If
        intSec = time Mod 60
        If intSec < 10 Then
            strSec = "0" & intSec.ToString
        Else
            strSec = intSec.ToString
        End If
        strTemp = "Time Left: " & strHr & ":" & strMin & ":" & strSec
        Return strTemp
    End Function
```
The function for setting/disabling the timer and the function for displaying the time left.

##Returning the user's input to the command line

```
 Private Sub Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim selectedBtn As Button = sender

        Dim strOutput As String = selectedBtn.Text

        Console.Write(strOutput)
        boolResponse = True
        Me.Close()
    End Sub
```
Gets the button clicked by the user and returns its text to the command line.

```
Private Sub FrmCustomUserPrompt_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If boolResponse = False Then
            Console.Write("User closed the program or time ran out.")
            If boolBtnAdded = False Then
                Console.Write("  Not that it matters, since no buttons were added anyway.")
            End If
        End If
    End Sub
```
Checks if a button was clicked before the form closes.  If no button was clicked, returns a message to the command line.