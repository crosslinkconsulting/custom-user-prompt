#Custom User Prompt Instructions

To customize this form, use the following parameters followed by the value for each surrounded by quotation marks:

* **-hdr**: Change the header text
* **-text**: Set the body text.  Include \n to start a new line.
* **-btn**: Add button(s) to the form.  Include \n to add different buttons
* **-img**: Display an image from a file path.
* **-icon**: Set a custom form icon from a file path.
* **-time**: Set the time in seconds until the form expires.  Must be a whole number.  Set to 0 to disable timer
* **-height**: Set the height of the form.  Must be a whole number greater than 170
* **-width**: Set the width of the form.  Must be a whole number greater than 300
* **-ontop**:  Set to 1 to force the form on top of other windows
* **-noxbtn**:  Set to 1 to disable the X button on the form.

Example: start /WAIT /B (path to program) -hdr "Example Header" -text "Line 1\nLine 2" -height "250" -btn "Yes\nNo"

A .exe build can be found under *CustomUserPrompt/bin/Debug/CustomUserPrompt.exe*

![Example of the form](ExampleImage.jpg)

* [Code Examples](codeexamples.md)

One thing I had to figure out was how to return text from the buttons to the command line.  One of the project requirements was figuring out which button the user clicked by returning its text to the command line.  When we started out, we couldn't seem to find the answer, but we discovered that the project was set up as a GUI application when it needed to be a console application.

In addition to that, we called the .exe using the *start /WAIT /B* command, which allows the command line to wait for the .exe to finish running before moving on, allowing us to get the input from the user before the application closed.

This program was written during my 2019 summer internship at *Crosslink Consulting* for the primary purpose of being used in prompting users for reboots.

The program is designed to be modular and accept command line arguments.